#!/usr/bin/env php
<?php
require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

(new Application('Test Task', '1.0.0'))
    ->register('start')
    ->addArgument('filename', InputArgument::OPTIONAL, 'Filename')
    ->addArgument('action', InputArgument::OPTIONAL, 'Arithmetic action')
    ->setCode(function (InputInterface $input, OutputInterface $output) {

        $filename = $input->getArgument('filename');
        $action = $input->getArgument('action');

        if (!empty($filename) || !empty($action)) {
            if (file_exists($filename)) {
                $output->writeln("please waiting...");
                $filePositive = fopen( 'positive.txt', 'w' );
                $fileNegative = fopen( 'negative.txt', 'w' );
                $strList = file($filename);
                foreach ($strList as $str) {
                    $strToArray = explode(' ', $str);
                    if (count($strToArray) == 2) {
                        $firstElement = (int)$strToArray[0];
                        $secondElement = (int)$strToArray[1];
                        if(is_int($firstElement) && is_int($secondElement)){
                            $result = false;
                            switch ( $action ){
                                case('*'):
                                    $result = $firstElement * $secondElement;
                                    break;
                                case('/'):
                                    if($secondElement != 0){
                                        $result = $firstElement / $secondElement;
                                    }
                                    break;
                                case('+'):
                                    $result = $firstElement + $secondElement;
                                    break;
                                case('-'):
                                    $result = $firstElement - $secondElement;
                            }
                            if($result){
                                if($result>0){
                                    fwrite($filePositive, $result."\r\n");
                                }else{
                                    fwrite($fileNegative, $result."\r\n");
                                }
                            }
                        }
                    }
                }
                fclose($filePositive);
                fclose($fileNegative);
                echo $output->writeln("process is finished");
            } else {
                echo $output->writeln("The file $filename does not exist");
            }

        } else {
            echo $output->writeln("filename & action is required");
        }
    })
    ->getApplication()
    ->run();